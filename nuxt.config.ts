// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  modules: [
    "@pinia/nuxt",
    "@pinia-plugin-persistedstate/nuxt",
    "nuxt-vuefire",
    "@nuxtjs/ionic",
    '@invictus.codes/nuxt-vuetify'
  ],
  pinia: {
    autoImports: [
      // automatically imports `defineStore`
      "defineStore", // import { defineStore } from 'pinia'
    ],
  },
  piniaPersistedstate: {
    cookieOptions: {
      sameSite: "strict",
    },
    storage: "localStorage",
  },
  vuefire: {
    auth: true,
    admin: {
      serviceAccount: "./assets/checkrate-a43f8-firebase-adminsdk-lr1e7-eeb84fb075.json",
    },
    config: {
      apiKey: "AIzaSyAt936JCRt4ArOcbf-OHWc3NGU5YqYIh4Y",
      authDomain: "checkrate-a43f8.firebaseapp.com",
      projectId: "checkrate-a43f8",
      storageBucket: "checkrate-a43f8.appspot.com",
      messagingSenderId: "760987750720",
      appId: "1:760987750720:web:f4563170b710ddac506aa5",
      measurementId: "G-YT1XD68VL7",
      // there could be other properties depending on the project
    },
  },
  vuetify: {
    vuetifyOptions: {
      // @TODO: list all vuetify options
    },

    moduleOptions: {
      /* nuxt-vuetify module options */
      treeshaking: true,
      useIconCDN: true,

      /* vite-plugin-vuetify options */
      styles: true,
      autoImport: true,
      useVuetifyLabs: true, 
    }
  }
});
